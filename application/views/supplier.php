<?= $this->session->flashdata('pesan'); ?>
<div class="card shadow-sm border-bottom-primary">
    <div class="card-header bg-white py-3">
        <div class="row">
            <div class="col">
                <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                    Data Supplier
                </h4>
            </div>
            <?php        
                    $role= userdata('role');
            if($role == 'admin'){
                ?>  
            <div class="col-auto">
            <a href="#myModal" data-toggle="modal"  class="btn btn-sm btn-primary btn-icon-split">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">
                        Tambah Supplier
                    </span>
                </a>
            </div>
            <?php }
        ?>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped w-100 dt-responsive nowrap" id="dataTable">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Nomor Telepon</th>
                    <th>Alamat</th>
                    <th>NPWP</th>
                    <th>Spesifikasi</th>
                    <th>Harga</th>
                    <th>File</th>
                    <?php        
                    $role= userdata('role');
            if($role == 'admin'){
                ?>
                    <th>Aksi</th>
                    <?php }
        ?>
                </tr>
            </thead>
            <tbody>
            <?php
                    $no=0;
                    if ($data) :
  					foreach ($data->result_array() as $i) :
  					   $no++;
                       $id=$i['id_supplier'];
                       $nama=$i['nama_supplier'];
                       $tanggal=$i['tanggal'];
                       $notelp=$i['no_telp'];
                       $alamat=$i['alamat'];
                       $npwp=$i['npwp'];
                       $spesifikasi=$i['spesifikasi'];
                       $harga=$i['harga'];
                       $file=$i['brosur'];
                    ?>
                        <tr>
                            <td><?php echo $no;?></td>
                            <td><?php echo $nama;?></td>
                            <td><?php echo $notelp;?></td>
                            <td><?php echo $alamat;?></td>
                            <td><?= $i['npwp']; ?></td>
                            <td><?= $i['spesifikasi']; ?></td>
                            <td><?= $i['harga']; ?></td>
                            <td><a class="btn btn-primary" title="<?php echo $file;?>" target="_blank" href="<?= base_url('')?>assets/files/supplier/<?php echo $file;?>">DOWNLOAD FILE</a></td></td>
                            <?php        
                    $role= userdata('role');
            if($role == 'admin'){
                ?>
                            <td>
                                <a class="btn btn-warning btn-circle btn-sm" title="Edit Data" data-toggle="modal" href="#ModalEdit<?php echo $id;?>"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger btn-circle btn-sm" title="Hapus Data" data-toggle="modal"  href="#ModalHapus<?php echo $id;?>"><i class="fa fa-trash"></i></a>
                            </td>
                            <?php }
        ?>    
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="6" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

<!-- -------------------------------------------------- -->
<!-- Add Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Pengadaan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body --><form class="form-horizontal" action="<?php echo base_url().'supplier/simpan_file'?>" method="post" enctype="multipart/form-data">
<div class="modal-body">
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">  
        <div class="card-body">
      

        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Nama Perusahaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-book"></i></span>
                            </div>
                            <input type="text" name="xnama" class="form-control" id="inputUserName" placeholder="nama perusahaan" required>
                        </div>
                        
                    </div>
        </div>
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">No Telp</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-user"></i></span>
                            </div>
                            <input type="text" name="xnotelp" class="form-control" id="inputUserName" placeholder="nomor telephone" required>
                        </div>
                        
                    </div>
        </div>  
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Alamat Peruhsaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-map-pin"></i></span>
                            </div>
                            <input type="text" name="xalamat" class="form-control" id="inputUserName" placeholder="alamat peruhsaan" required>
                        </div>
                        
                    </div>
        </div> 
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Npwp Perusahaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-id-card"></i></span>
                            </div>
                            <input type="text" name="xnpwp" class="form-control" id="inputUserName" placeholder="npwp peruhsahaan" required>
                        </div>
                        
                    </div>
        </div>  
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Spesifikasi</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-archive"></i></span>
                            </div>
                            <input type="text" name="xspesifikasi" class="form-control" id="inputUserName" placeholder="spesifikasi" required>
                        </div>
                        
                    </div>
        </div>  
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Harga</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-credit-card"></i></span>
                            </div>
                            <input type="text" name="xharga" class="form-control" id="inputUserName" placeholder="harga" required>
                        </div>
                        
                    </div>
        </div>  
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Brosur</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="brosur"  required>
                        </div>
                        
                    </div>
        </div>                                       
                            
                           
                        
                            
</div>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
      </div>
      </form>                        
    </div>
  </div>
</div>

<!-- -------------------------------------------------- -->
<?php foreach ($data->result_array() as $i) :
                $id=$i['id_supplier'];
                $nama=$i['nama_supplier'];
                $tanggal=$i['tanggal'];
                $file=$i['brosur'];
                
            ?>
	<!--Modal Hapus-->
        <div class="modal fade" id="ModalHapus<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Hapus Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'supplier/hapus_file'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">  
                    <div class="modal-body">
							<input type="hidden" name="kode" value="<?php echo $id;?>"/>
                            <input type="hidden" name="file" value="<?php echo $file;?>">
                            <p>Apakah Anda yakin mau menghapus file <b><?php echo $nama;?></b> ?</p>

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>

<!-- ----------------------------------------------------- -->
<?php foreach ($data->result_array() as $i) :
               $id=$i['id_supplier'];
               $nama=$i['nama_supplier'];
               $tanggal=$i['tanggal'];
               $notelp=$i['no_telp'];
               $alamat=$i['alamat'];
               $npwp=$i['npwp'];
               $spesifikasi=$i['spesifikasi'];
               $harga=$i['harga'];
               $file=$i['brosur'];
            ?>
	<!--Modal Edit Pengguna-->
        <div class="modal fade" id="ModalEdit<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Supplier</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'supplier/update_file'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Nama Perusahaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-book"></i></span>
                            </div>
                            <input type="text" name="xnama" class="form-control" value="<?php echo $nama;?>" id="inputUserName"required>
                        </div>
                        
                    </div>
        </div>
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">No Telp Peruhsaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-user"></i></span>
                            </div>
                            <input type="text" name="xnotelp" class="form-control" value="<?php echo $notelp;?>" id="inputUserName"required>
                        </div>
                        
                    </div>
        </div>
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Alamat</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-map-pin"></i></span>
                            </div>
                            <input type="text" name="xalamat" class="form-control" value="<?php echo $alamat;?>" id="inputUserName"required>
                        </div>
                        
                    </div>
        </div>
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">NPWP Perusahaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-id-card"></i></span>
                            </div>
                            <input type="text" name="xnpwp" class="form-control" value="<?php echo $npwp;?>" id="inputUserName"required>
                        </div>
                        
                    </div>
        </div>
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Spesifikasi</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-archive"></i></span>
                            </div>
                            <input type="text" name="xspesifikasi" class="form-control" value="<?php echo $spesifikasi;?>" id="inputUserName"required>
                        </div>
                        
                    </div>
        </div>
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Harga</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-credit-card"></i></span>
                            </div>
                            <input type="text" name="xharga" class="form-control" value="<?php echo $harga;?>" id="inputUserName"required>
                        </div>
                        
                    </div>
        </div>
      
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Brosur</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="brosur" >
                        </div>
                        
                    </div>
        </div>   
                

    </div>
                            

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>
