<?= $this->session->flashdata('pesan'); ?>
<div class="card shadow-sm border-bottom-primary">
    <div class="card-header bg-white py-3">
        <div class="row">
            <div class="col">
                <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                    Data Pengadaan
                </h4>
            </div>
            <?php        
                    $role= userdata('role');
            if($role == 'kasih_pengada'){
                ?>                
            <div class="col-auto">
                <a href="#myModal" data-toggle="modal"  class="btn btn-sm btn-primary btn-icon-split">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">
                    
                        Tambah Pengadaaan
                    </span>
                </a>
            </div>
            <?php }
            elseif($role == 'admin'){
                ?>                
            <div class="col-auto">
                <a href="#myModal" data-toggle="modal"  class="btn btn-sm btn-primary btn-icon-split">
                    <span class="icon">
                        <i class="fa fa-plus"></i>
                    </span>
                    <span class="text">
                    
                        Tambah Pengadaaan
                    </span>
                </a>
            </div>
            <?php }
        ?>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped w-100 dt-responsive nowrap" id="dataTable">
            <thead>
                <tr>
                    <th>No. </th>
                    <th>Judul Pengadaan</th>
                    <th>Tanggal</th>
                    <th>Spesifikasi Pengadaan</th>
                    <th>Pemenang Lelang</th>
                    <th>Jumlah</th>
                    <th>Total Anggaran</th>
                    <th>Masa Waktu Kerja</th>
                    <th>Details</th>
                <?php        
                    $role= userdata('role');
                        if($role == 'kasih_pengada'){
                ?>
                    <th>Aksi</th> 
                <?php }
                        elseif($role == 'admin'){
                ?>
                    <th>Aksi</th> 
                <?php }
                
                ?>
                </tr>
            </thead>
            <tbody>
            <?php
                    $no=0;
                    if ($data) :
  					foreach ($data->result_array() as $i) :
  					   $no++;
                       $id=$i['id_pengadaan'];
                       $judul=$i['judul_pengadaan'];
                       $tanggal=$i['tanggal'];
                       $file=$i['spesifikasi_pengadaan'];
                       $pemenang=$i['pemenang_lelang'];
                       $unit=$i['jumlah_unit'];
                       $angaran=$i['angaran'];
                       $masa=$i['masa_kerja'];
                    ?>
                <tr>
                  <td><?php echo $no;?></td>
                  <td><?php echo $judul;?></td>
                  <td><?php echo date (" H:i - d F Y ", strtotime($tanggal));?></td>
                  <td><a class="btn btn-primary" title="<?php echo $file;?>" target="_blank" href="<?= base_url('')?>assets/files/pengadaan/<?php echo $file;?>">DOWNLOAD FILE</a></td>
                  <td><?php echo $pemenang;?></td>
                  <td><?php echo $unit;?> unit</td>
                  <td>Rp. <?php echo $angaran;?></td>
                  <td><?php echo $masa;?></td> 
                  <td><a class="btn btn-warning btn-circle btn-sm" title="Detail Data" data-toggle="modal" href="#ModalView<?=$i['id_pengadaan'] ?>"><i class="fa fa-eye"></i></a></td>
                  <?php        
                    $role= userdata('role');
                        if($role == 'kasih_pengada'){
                ?>
                            <td>
                                <a class="btn btn-warning btn-circle btn-sm" title="Edit Data" data-toggle="modal" href="#ModalEdit<?php echo $id;?>"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger btn-circle btn-sm" title="Hapus Data" data-toggle="modal"  href="#ModalHapus<?php echo $id;?>"><i class="fa fa-trash"></i></a>
                            </td>
                            <?php }
                        elseif($role == 'admin'){
                ?>
                            <td>
                                <a class="btn btn-warning btn-circle btn-sm" title="Edit Data" data-toggle="modal" href="#ModalEdit<?php echo $id;?>"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger btn-circle btn-sm" title="Hapus Data" data-toggle="modal"  href="#ModalHapus<?php echo $id;?>"><i class="fa fa-trash"></i></a>
                            </td>
                            <?php }    
                
                ?>
                
                </tr>
                        <?php endforeach; ?>
                        <?php else : ?>
                    <tr>
                        <td colspan="7" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?> 
            </tbody>
           
        </table>
    </div>
</div>

<?php
    foreach ($data->result_array() as $i) : ?>


<!-- more Modal -->
<div class="modal fade" id="ModalView<?=$i['id_pengadaan'] ?>">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Detail Pengadaan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      <div class="table-responsive">
        <table class="table table-striped w-100 dt-responsive nowrap" id="dataTable">
            <thead>
                <tr>
                    <th>Dokumen</th>
                    <th>File</th>
                    <th>Download</th>
                    <th>aksi</th> 
                </tr>
            </thead>
            <tbody>            
            
            <tr>
            <td>Dokumen Data Dukung</td>    
            <td><?= $i['datadukung_pengadaan']; ?></td>
            <td><a href="<?= base_url('')?>assets/files/pengadaan/<?php echo $i['datadukung_pengadaan'];?>" target="_blank" class="btn btn-circle btn-info btn-sm"><i class="fa fa-download"></i></a>   </td>           
            <td>
            <?php 
                                $role= userdata('role');
                                if($role == 'kasih_pengada'){
                                   ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#ModalData<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                               
                               <?php  
                                                        
                            } elseif($role == 'admin'){
                                ?>
                             <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#ModalData<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>    
                                
                                <?php 
                            }
                            
                                ?>
                                
            </td>
            </tr>

            <td>Dokumen Harga Perkiraan</td>                
            <td><?= $i['dokumen_harga']; ?></td>
            <td><a href="<?= base_url('')?>assets/files/pengadaan/<?php echo $i['dokumen_harga'];?>" target="_blank" class="btn btn-circle btn-info btn-sm"><i class="fa fa-download"></i></a>   </td>              
            <td>
            <?php 
                                $role= userdata('role');
                                if($role == 'ppk'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalharga<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'admin'){
                                    ?>
                                 <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalharga<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>    
                                    
                                    <?php 
                                }
                                ?>
            </td>
            </tr>

            <td>Pengumuman Pengadaan</td>                
            <td><?= $i['pengumuman_pengadaan']; ?></td>
            <td><a href="<?= base_url('')?>assets/files/pengadaan/<?php echo $i['pengumuman_pengadaan'];?>" target="_blank" class="btn btn-circle btn-info btn-sm"><i class="fa fa-download"></i></a>   </td>              
            <td>
            <?php 
                                $role= userdata('role');
                                if($role == 'pokja'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalpengumuman<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'admin'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalpengumuman<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } 
                                ?>
            </td>
            </tr>

            <td>Pengumuman Pemenang</td>                
            <td><?= $i['pengumuman_pemenang']; ?></td>
            <td><a href="<?= base_url('')?>assets/files/pengadaan/<?php echo $i['pengumuman_pemenang'];?>" target="_blank" class="btn btn-circle btn-info btn-sm"><i class="fa fa-download"></i></a>   </td>              
            <td>
            <?php 
                                $role= userdata('role');
                                if($role == 'pokja'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalpemenang<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } if($role == 'admin'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalpemenang<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                }
                                ?>
            </td>
            </tr>

            <td>Usulan Pemenang dari Pokja</td>                
            <td><?= $i['usulan_pemenang']; ?></td>
            <td><a href="<?= base_url('')?>assets/files/pengadaan/<?php echo $i['usulan_pemenang'];?>" target="_blank" class="btn btn-circle btn-info btn-sm"><i class="fa fa-download"></i></a>   </td>              
            <td>
            <?php 
                                $role= userdata('role');
                                if($role == 'pokja'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalusulan<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'admin'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalusulan<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } 
                                ?>
            </td>
            </tr>

            <td>Pengumuman Pemenang yang Dipilih PPK</td>                
            <td><?= $i['pengumuman_pemenang_ppk']; ?></td>
            <td><a href="<?= base_url('')?>assets/files/pengadaan/<?php echo $i['pengumuman_pemenang_ppk'];?>" target="_blank" class="btn btn-circle btn-info btn-sm"><i class="fa fa-download"></i></a>   </td>              
            <td>
            <?php 
                                $role= userdata('role');
                                if($role == 'ppk'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalpemenangppk<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'admin'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalpemenangppk<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                }
                                ?>
            </td>
            </tr>

            <td>Kontrak PPK dengan Pemenang</td>                
            <td><?= $i['kontrak_ppk']; ?></td>
            <td><a href="<?= base_url('')?>assets/files/pengadaan/<?php echo $i['kontrak_ppk'];?>" target="_blank" class="btn btn-circle btn-info btn-sm"><i class="fa fa-download"></i></a>   </td>              
            <td>
            <?php 
                                $role= userdata('role');
                                if($role == 'ppk'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalkontrak<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'admin'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalkontrak<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                }
                                ?>
            </td>
            </tr>

            <td>SPK PPK dengan Pemenang</td>                
            <td><?= $i['spk_ppk']; ?></td>
            <td><a href="<?= base_url('')?>assets/files/pengadaan/<?php echo $i['spk_ppk'];?>" target="_blank" class="btn btn-circle btn-info btn-sm"><i class="fa fa-download"></i></a>   </td>              
            <td>
            <?php 
                                $role= userdata('role');
                                if($role == 'ppk'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalspk<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'admin'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalspk<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                }
                                ?>
            </td>
            </tr>
            
            <td>laporan Hasil Pekerjaan</td>                
            <td><?= $i['laporan_hasil']; ?></td>
            <td><a href="<?= base_url('')?>assets/files/pengadaan/<?php echo $i['laporan_hasil'];?>" target="_blank" class="btn btn-circle btn-info btn-sm"><i class="fa fa-download"></i></a>   </td>              
            <td>
            <?php 
                                $role= userdata('role');
                                if($role == 'panitia'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalhasil<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'ppk'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalhasil<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'admin'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalhasil<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                }
                                ?>
            </td>
            </tr>

            <td>laporan Addendum</td>                
            <td><?= $i['laporan_addendum']; ?></td>
            <td><a href="<?= base_url('')?>assets/files/pengadaan/<?php echo $i['laporan_addendum'];?>" target="_blank" class="btn btn-circle btn-info btn-sm"><i class="fa fa-download"></i></a>   </td>              
            <td>
            <?php 
                                $role= userdata('role');
                                if($role == 'ppk'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modaladdendum<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'pokja'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modaladdendum<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'panitia'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modaladdendum<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'admin'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modaladdendum<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                }
                                ?>
            </td>
            </tr>

            <td>laporan Akhir</td>                
            <td><?= $i['laporan_akhir']; ?></td>
            <td><a href="<?= base_url('')?>assets/files/pengadaan/<?php echo $i['laporan_akhir'];?>" target="_blank" class="btn btn-circle btn-info btn-sm"><i class="fa fa-download"></i></a>   </td>              
            <td>
            <?php 
                                $role= userdata('role');
                                if($role == 'ppk'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalakhir<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'panitia'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalakhir<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'admin'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalakhir<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                }
                                ?>
            </td>
            </tr>

            <td>Berita Acara</td>                
            <td><?= $i['berita_acara']; ?></td>
            <td><a href="<?= base_url('')?>assets/files/pengadaan/<?php echo $i['berita_acara'];?>" target="_blank" class="btn btn-circle btn-info btn-sm"><i class="fa fa-download"></i></a>   </td>              
            <td>
            <?php 
                                $role= userdata('role');
                                if($role == 'panitia'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalacara<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'ppk'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalacara<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'admin'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modalacara<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                }
                                ?>
            </td>
            </tr>

            <td>Risalah Dokumen</td>                
            <td><?= $i['risalah_dokumen']; ?></td>
            <td><a href="<?= base_url('')?>assets/files/pengadaan/<?php echo $i['risalah_dokumen'];?>" target="_blank" class="btn btn-circle btn-info btn-sm"><i class="fa fa-download"></i></a>   </td>              
            <td>
            <?php 
                                $role= userdata('role');
                                if($role == 'ppk'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modaldokumen<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                } elseif($role == 'admin'){
                                    ?>
                                <a class="btn btn-danger btn-circle btn-sm" title="Tambah Data" data-toggle="modal" href="#Modaldokumen<?=$i['id_pengadaan'] ?>"><i class="fa fa-plus"></i></a>
                                <?php 
                                }
                                ?>
            </td>
            </tr>
                    
                         
            </tbody>
        </table>
    </div>





      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
      </form>                        
    </div>
  </div>
</div>
<?php endforeach; ?>

<!-- Add Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Add Pengadaan</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body --><form class="form-horizontal" action="<?php echo base_url();?>simpan" method="post" enctype="multipart/form-data">
<div class="modal-body">
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">  
        <div class="card-body">
      

        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Judul Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-book"></i></span>
                            </div>
                            <input type="text" name="xjudul" class="form-control" id="inputUserName" placeholder="Judul" required>
                        </div>
                        
                    </div>
        </div>
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Pemenang Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-user"></i></span>
                            </div>
                            <input type="text" name="xpemenang" class="form-control" id="inputUserName" placeholder="pemenang lelang" required>
                        </div>
                        
                    </div>
        </div>  
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Jumlah</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-archive"></i></span>
                            </div>
                            <input type="text" name="xunit" class="form-control" id="inputUserName" placeholder="Jumlah lelang" required>
                        </div>
                        
                    </div>
        </div> 
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Angaran</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-credit-card"></i></span>
                            </div>
                            <input type="text" name="xangaran" class="form-control" id="inputUserName" placeholder="Jumlah lelang" required>
                        </div>
                        
                    </div>
        </div>  
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Masa Kerja</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-clock"></i></span>
                            </div>
                            <input type="text" name="xmasa" class="form-control" id="inputUserName" placeholder="Masa Kerja" required>
                        </div>
                        
                    </div>
        </div>  
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Spesifikasi Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                        
                    </div>
        </div>                                       
                            
                           
                        
                            
</div>



      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Simpan</button>
      </div>
      </form>                        
    </div>
  </div>
</div>







<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['spesifikasi_pengadaan'];
                
            ?>
	<!--Modal Hapus-->
        <div class="modal fade" id="ModalHapus<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Hapus Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/hapus_file'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">  
                    <div class="modal-body">
							<input type="hidden" name="kode" value="<?php echo $id;?>"/>
                            <input type="hidden" name="file" value="<?php echo $file;?>">
                            <p>Apakah Anda yakin mau menghapus file <b><?php echo $judul;?></b> ?</p>

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Hapus</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>


	<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                       $tanggal=$i['tanggal'];
                       $file=$i['spesifikasi_pengadaan'];
                       $pemenang=$i['pemenang_lelang'];
                       $unit=$i['jumlah_unit'];
                       $angaran=$i['angaran'];
                       $masa=$i['masa_kerja'];
            ?>
	<!--Modal Edit Pengguna-->
        <div class="modal fade" id="ModalEdit<?php echo $id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_file'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Judul Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-book"></i></span>
                            </div>
                            <input type="text" name="xjudul" class="form-control" value="<?php echo $judul;?>" id="inputUserName"required>
                        </div>
                        
                    </div>
        </div>
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Pemenang Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-user"></i></span>
                            </div>
                            <input type="text" name="xpemenang" class="form-control" value="<?php echo $pemenang;?>" id="inputUserName"required>
                        </div>
                        
                    </div>
        </div>
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Unit</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-archive"></i></span>
                            </div>
                            <input type="text" name="xunit" class="form-control" value="<?php echo $unit;?>" id="inputUserName"required>
                        </div>
                        
                    </div>
        </div>
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Angaran</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-credit-card"></i></span>
                            </div>
                            <input type="text" name="xangaran" class="form-control" value="<?php echo $angaran;?>" id="inputUserName"required>
                        </div>
                        
                    </div>
        </div>
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Waktu Kerja</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-clock"></i></span>
                            </div>
                            <input type="text" name="xmasa" class="form-control" value="<?php echo $masa;?>" id="inputUserName"required>
                        </div>
                        
                    </div>
        </div>
      
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Spesifikasi Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto">
                        </div>
                        
                    </div>
        </div>   
                

    </div>
                            

                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>




   

<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['datadukung_pengadaan'];
            ?>
	<!--Modal Data Dukung Pengguna-->
        <div class="modal fade" id="ModalData<?=$i['id_pengadaan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_dukung'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Data Dukung Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                    </div>
        </div>                
    </div>
                            

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>

<!-- ------------------------------------------------------------------------------------------------- -->

    <?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['dokumen_harga'];
            ?>
	<!--Modal harga Pengguna-->
        <div class="modal fade" id="Modalharga<?=$i['id_pengadaan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_harga'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Dokumen Harga Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                    </div>
        </div>                
    </div>
                            

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>



<!-- ------------------------------------------------------------------------------------------------- -->

<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['pengumuman_pengadaan'];
            ?>
	<!--Modal pengumuman Pengguna-->
        <div class="modal fade" id="Modalpengumuman<?=$i['id_pengadaan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_pengumuman'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Pengumuman Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                    </div>
        </div>                
    </div>
                            

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>


<!-- ------------------------------------------------------------------------------------------------- -->

<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['pengumuman_pemenang'];
            ?>
	<!--Modal pemenang Pengguna-->
        <div class="modal fade" id="Modalpemenang<?=$i['id_pengadaan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_pemenang'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Pengumuman Pemenang</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                    </div>
        </div>                
    </div>
                            

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>


<!-- ------------------------------------------------------------------------------------------------- -->

<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['usulan_pemenang'];
            ?>
	<!--Modal usulan Pengguna-->
        <div class="modal fade" id="Modalusulan<?=$i['id_pengadaan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_usulan'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">usulan Pemenang</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                    </div>
        </div>                
    </div>
                            

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>


<!-- ------------------------------------------------------------------------------------------------- -->

<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['pengumuman_pemenang_ppk'];
            ?>
	<!--Modal pemenangppk Pengguna-->
        <div class="modal fade" id="Modalpemenangppk<?=$i['id_pengadaan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_pemenangppk'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">pengumuman Pemenang oleh PPK</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                    </div>
        </div>                
    </div>
                            

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>

<!-- ------------------------------------------------------------------------------------------------- -->

<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['kontrak_ppk'];
            ?>
	<!--Modal kontrak Pengguna-->
        <div class="modal fade" id="Modalkontrak<?=$i['id_pengadaan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_kontrak'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Kontrak PPK</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                    </div>
        </div>                
    </div>
                            

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>


<!-- ------------------------------------------------------------------------------------------------- -->

<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['spk_ppk'];
            ?>
	<!--Modal spk Pengguna-->
        <div class="modal fade" id="Modalspk<?=$i['id_pengadaan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_spk'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">SPK PPK</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                    </div>
        </div>                
    </div>
                            

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>


<!-- ------------------------------------------------------------------------------------------------- -->

<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['laporan_hasil'];
            ?>
	<!--Modal hasil Pengguna-->
        <div class="modal fade" id="Modalhasil<?=$i['id_pengadaan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_hasil'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">laporan Hasil Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                    </div>
        </div>                
    </div>
                            

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>

<!-- ------------------------------------------------------------------------------------------------- -->

<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['laporan_addendum'];
            ?>
	<!--Modal addendum Pengguna-->
        <div class="modal fade" id="Modaladdendum<?=$i['id_pengadaan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_addendum'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">laporan Addendum Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                    </div>
        </div>                
    </div>
                            

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>

<!-- ------------------------------------------------------------------------------------------------- -->

<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['laporan_akhir'];
            ?>
	<!--Modal akhir Pengguna-->
        <div class="modal fade" id="Modalakhir<?=$i['id_pengadaan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_akhir'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">laporan Akhir Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                    </div>
        </div>                
    </div>
                            

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>

<!-- ------------------------------------------------------------------------------------------------- -->

<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['berita_acara'];
            ?>
	<!--Modal acara Pengguna-->
        <div class="modal fade" id="Modalacara<?=$i['id_pengadaan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_acara'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Berita Acara Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                    </div>
        </div>                
    </div>
                            

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>

<!-- ------------------------------------------------------------------------------------------------- -->

<?php foreach ($data->result_array() as $i) :
                $id=$i['id_pengadaan'];
                $judul=$i['judul_pengadaan'];
                $tanggal=$i['tanggal'];
                $file=$i['risalah_dokumen'];
            ?>
	<!--Modal dokumen Pengguna-->
        <div class="modal fade" id="Modaldokumen<?=$i['id_pengadaan'] ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="Edit">Edit Pengadaan</h4>
                    </div>
                    <form class="form-horizontal" action="<?php echo base_url().'pengadaan/update_dokumen'?>" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                    <div class="modal-body">
    <div class="card-body">
      
        
        <div class="row form-group">
                    <label class="col-md-3 text-md-right">Risalah Dokumen Pengadaan</label>
                    <div class="col-md-9">
                        <div class="input-group">
                        <input type="hidden" name="kode" value="<?php echo $id;?>">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><i class="fa fa-fw fa-file"></i></span>
                            </div>
                            <input type="file" name="filefoto"  required>
                        </div>
                    </div>
        </div>                
    </div>
                            

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary btn-flat" id="simpan">Update</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
	<?php endforeach;?>

