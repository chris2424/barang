<!DOCTYPE html>
<html>
<head>
<title>Portal Direktorat Logistik</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Unitary Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	SmartPhone Compatible web template, free Web designs for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="css/style.css" type="text/css" rel="stylesheet" media="all"> 
<link rel="stylesheet" type="text/css" href="css/menu_topexpand.css" />
<link rel="stylesheet" href="css/percircle.css"> 
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<!-- //Custom Theme files -->
<!-- js -->
<script src="js/jquery-2.2.3.min.js"></script> 
<script src="js/SmoothScroll.min.js"></script>
<!-- //js -->
<style>
/* Button styles */
.show-card {
  animation: fade-in 0.4s 1s forwards ease-out;
  background: #fff;
  border-radius: 4em;
  border: 0.25em solid #1fa756;
  color: #1fa756;
  font-size: 18px;
  left: 50%;
  line-height: 1.5;
  opacity: 0;
  padding: 1em 1em 1em 4em;
  position: absolute;
  top: 50%;
  transform: translate(-50%, -50%);
  outline: none;

}

.show-card.hide {
  animation: fade-out 0.4s forwards ease-out;
}
/* Image on the button */
.show-card img {
  border-radius: 50%;
  left: 0.3em;
  position: absolute;
  top: 0.25em;
  width: 3em;
}

.card {
  bottom: calc(50% - 8em);
  display: none;
  height: 16em;
  left: calc(50% - 10em);
  position: absolute;
  transition: all 0.4s 0.4s cubic-bezier(.5,.2,.43,1.33);
  width: 20em;
}

.close {
  color: #aaa;
  cursor: pointer;
  height: 1em;
  opacity: 0;
  position: absolute;
  right: 0.5em;
  top: 0.5em;
  width: 1em;
  z-index: 10;
}

.details {
  position: absolute;
  bottom: 2.5em;
  background: #fff;
  border-radius: 0.25em;
  height: 0;
  overflow: hidden;
  text-align: center;
  width: 20em;
}

.name {
  color: #333;
  font-weight: bold;
  opacity: 0;
  margin: 3em 0 0;
}

.description {
  color: #666;
  font-size: 1em;
  font-weight: 200;
  line-height: 1.5;
  opacity: 0;
  margin: 0.75em 2em 2em;
}

.headshot img {
  border-radius: 50%;
  border: 0.5em solid #fff;
  display: block;
  height: 6em;
  margin: -3em auto 0.5em;
  width: 6em;
  opacity: 0;
  transform: translateZ(0);
}

.icon-bar {
  background:#f35a5a;
  border-radius: 0.25em;
  height: 4em;
  left: -1em;
  position: absolute;
  right: -1em;
  top: 13em;
}

.icon-bar ul {
  display: flex;
  flex-display: column;
  flex-wrap: nowrap;
  padding: 0;
}

.icon-bar li {
  display: inline-block;
  font-size: 2em;
  padding: 0 1em;
  opacity: 0;
  width: 33%;
}

.icon-bar a {
  color: #fff;
  text-decoration: none;
}

/* ANIMATIONS */

/* Show state */

.card.show .details {
  animation: show-detail-container 0.7s 0.55s forwards cubic-bezier(.54,-0.38,.34,1.42);
}
.card.show .name {
  animation: fade-in 0.3s 1.3s forwards, pop-in 0.3s 1.3s forwards cubic-bezier(.64,1.87,.64,.64);
}
.card.show  .description {
  animation: fade-in 0.3s 1.4s forwards, pop-in 0.3s 1.4s forwards cubic-bezier(.64,1.87,.64,.64);
}
.card.show .headshot img {
  animation: fade-in 0.4s 1.2s forwards, pop-in 0.6s 1.2s forwards cubic-bezier(.64,1.87,.64,.64);
}
.card.show .icon-bar {
  animation: show-icon-bar .5s forwards cubic-bezier(.64,1.87,.64,.64);
}

.card.show .icon-bar li {
  animation: show-icon 0.5s forwards cubic-bezier(.64,1.87,.64,.64);
}

.card.show .icon-bar li:nth-child(1) {
  animation-delay: 1.8s;
}
.card.show .icon-bar li:nth-child(2) {
  animation-delay: 1.9s;
}
.card.show .icon-bar li:nth-child(3) {
  animation-delay: 2s;
}

.card.show .close {
  animation: fade-in 0.3s 1.3s forwards;
}

/* Hide state */

.card.hide {
  animation: drop-card 1s forwards cubic-bezier(.54,-0.38,.34,1.42);
}

/* Animations */

@keyframes fade-in {
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}

@keyframes fade-out {
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
}

@keyframes show-icon-bar {
  0% {
    height: 0;
  }
  100% {
    height: 4em;
  }
}

@keyframes show-icon {
  0% {
    opacity: 0;
    transform: rotateZ(-40deg);
  }
  100% {
    opacity: 1;
    transform: rotateZ(0);
  }
}

@keyframes show-detail-container {
  0% {
    height: 0;
  }
  100% {
    height: 13.5em;
  }
}

@keyframes pop-in {
  0% {
    transform: scale(0.7);
  }
  100% {
    transform: scale(1)
  }
}

@keyframes drop-card {
  100% {
    bottom: -100%;
    transform: rotateZ(20deg);
    opacity: 0;
  }
}

</style>
</head>
 <link rel="shortcut icon" href="images/bnpbb.png">

<body>
		
		<!-- team -->
		<div id="team" class="team">
			<div class="container">

			<center><img class="img-responsive" src="images/bnpbb.png" style="width: 130px; height: 130px; " alt="Logo"></center>
				<h3 class="agileits-title"></h3><h3 class="text-danger" style="font-size: 60px;"><u>Selamat <span class="text-info">Datang</span></u></h3>
				<h4 class="agileits-title"></h4><h3 class="text-primary text-left" style="font-size: 20px;">Web Portal Direktorat Logistik</h3>
				<br>
				<br>
				<h3  class="text-primary" style="margin-top:20px;font-size:40px; text-align: center;">Badan Nasional Penanggulangan Bencana</h3>  
				<br>
			
				<div class="agile_team_grids"> 
					<div class="col-md-4 agile_team_grid">
						<div class="ih-item circle effect1">
							<div class="spinner"></div>
							<a title="Aplikasi" href="http://Logistik.bnpb.go.id"><div class="img"><img src="images/klient.png" alt=" " class="img-responsive"></div>
								<div class="info-back">
								  <h4><div style="font-size:15px;">Aplikasi 1</div></h4>
								  
								</div>
								</a>
							</div>
						</div>
					
					</div>
					<div class="col-md-4 agile_team_grid">
						<div class="ih-item circle effect1">
							<div class="spinner"></div>
							   <a title="Aplikasi" href="http://Logistik.bnpb.go.id"><div class="img"><img src="images/klient.png" alt=" " class="img-responsive"></div>
							
								<div class="info-back">
								  <h4><div style="font-size:15px;">Aplikasi 2 </div></h4>
								  
								</div>
							</a>
						</div>
			
					</div>
					<div class="col-md-4 agile_team_grid">
						<div class="ih-item circle effect1">
							<div class="spinner"></div>
							   <a title="Aplikasi" href="http://Logistik.bnpb.go.id"><div class="img"><img src="images/man_icon.png" alt=" " class="img-responsive"></div>
							
								<div class="info-back">
								  <h4><div style="font-size:15px;">Aplikasi 3</div></h4>
								  
								</div>
								
							</a>
						</div>
						
					
					</div>
					<button class="show-card text-right" style="margin-top: 320px;">
							<img src="images/bnpbb.png">
							Klik Disini
						  </button>
						  <article class="card">
							<section class="close">
							  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="14" height="14" viewBox="0 0 32 32">
								<path d="M31.708 25.708c-0-0-0-0-0-0l-9.708-9.708 9.708-9.708c0-0 0-0 0-0 0.105-0.105 0.18-0.227 0.229-0.357 0.133-0.356 0.057-0.771-0.229-1.057l-4.586-4.586c-0.286-0.286-0.702-0.361-1.057-0.229-0.13 0.048-0.252 0.124-0.357 0.228 0 0-0 0-0 0l-9.708 9.708-9.708-9.708c-0-0-0-0-0-0-0.105-0.104-0.227-0.18-0.357-0.228-0.356-0.133-0.771-0.057-1.057 0.229l-4.586 4.586c-0.286 0.286-0.361 0.702-0.229 1.057 0.049 0.13 0.124 0.252 0.229 0.357 0 0 0 0 0 0l9.708 9.708-9.708 9.708c-0 0-0 0-0 0-0.104 0.105-0.18 0.227-0.229 0.357-0.133 0.355-0.057 0.771 0.229 1.057l4.586 4.586c0.286 0.286 0.702 0.361 1.057 0.229 0.13-0.049 0.252-0.124 0.357-0.229 0-0 0-0 0-0l9.708-9.708 9.708 9.708c0 0 0 0 0 0 0.105 0.105 0.227 0.18 0.357 0.229 0.356 0.133 0.771 0.057 1.057-0.229l4.586-4.586c0.286-0.286 0.362-0.702 0.229-1.057-0.049-0.13-0.124-0.252-0.229-0.357z" fill="#aaa"></path>
							  </svg>
							</section>
							<section class="details">
							  <h2 class="name">Direktorat Logistik</h2>
							  <p class="description">Contact us</p>
							</section>
							<section class="headshot">
							  <img src="images/bnpbb.png">
							</section>
							<section class="icon-bar">
							  <ul>
								<li>
								  <a href="http://bnpb.go.id">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="32" height="32" viewBox="0 0 32 32">
									  <path d="M32 19l-6-6v-9h-4v5l-6-6-16 16v1h4v10h10v-6h4v6h10v-10h4z" fill="#fff"></path>
									</svg>
								  </a>
								</li>
								<li>
								  <a href="mailto:contact@bnpb.go.id">
										<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 32 32">
										<path d="M12 2.02c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm0 12.55l-5.992-4.57h11.983l-5.991 4.57zm0 1.288l-6-4.629v6.771h12v-6.771l-6 4.629z"fill="#fff">
											</path>
										</svg>
								  </a>
								</li>
								<li>
								  <a href="https://www.facebook.com/HumasBNPB/">
									<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 32 32"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-2 10h-2v2h2v6h3v-6h1.82l.18-2h-2v-.833c0-.478.096-.667.558-.667h1.442v-2.5h-2.404c-1.798 0-2.596.792-2.596 2.308v1.692z" fill="#fff">
									</path></svg>
								  </a>
								</li>
							</section>
						  </article>
					<div class="">
							   </div>
								<div class="info-back">
								
								  <h4><div style="font-size:15px;">&nbsp;</div></h4>  
								</div>
							<div class="">
							   </div>
								<div class="info-back">
								
								  <h4><div style="font-size:15px;">&nbsp;</div></h4>  
								</div>
							
									<div class="">
							   </div>
								<div class="info-back">
								
								  <h4><div style="font-size:15px;">&nbsp;</div></h4>  
								</div>
							
									<div class="">
							   </div>
								<div class="info-back">
								
								  <h4><div style="font-size:15px;">&nbsp;</div></h4>  
								</div>
							
									<div class="">
							   </div>
								<div class="info-back">
								
								  <h4><div style="font-size:15px;">&nbsp;</div></h4>  
								</div>
							
									
									
					
								

								
								
								
								
								
								</div>
							</a>
						
						
					</div>
					
					
					<div class="clearfix"> </div> 
				</div>
			
		
	
		<!-- //team -->
					<!-- footer -->
		<div class="footer">
			<div class="container">
				<p> <a href="http://www.bnpb.go.id" target="_blank">© 2019  - bnpb.go.id</a>  | Badan Nasional Penanggulangan Bencana</p>
				
			</div>
		</div>
		<!-- //footer -->
			
	<!-- //menu-js -->
	<script src="images/classie.js.download"></script>
	<script src="images/main.js.download"></script>
	<!-- //menu-js -->
	<!-- start-smooth-scrolling -->
	<script type="text/javascript" src="images/move-top.js.download"></script>
	<script type="text/javascript" src="images/easing.js.download"></script>	
	<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
			
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
	</script>
	<!-- //end-smooth-scrolling -->	 
	<!-- smooth-scrolling-of-move-up -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
			};
			*/
			
			$().UItoTop({ easingType: 'easeOutQuart' });
			
		});
	</script>
	<!-- //smooth-scrolling-of-move-up -->  
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.js"></script>
<script>

$('.show-card').click(function(e) {
  $('.card').addClass('show').css('display', 'block');
  $('.show-card').addClass('hide');
});

$('.card .close').click(function(e) {
  $('.card').addClass('hide');
  setTimeout(function() {
    $('.card').css('display', 'none').removeClass('show').removeClass('hide');
  }, 1000);
  $('.show-card').removeClass('hide');
});</script>


<a href="#" id="toTop" style="display: inline;"><span id="toTopHover"></span>To Top</a></body></html>