<?php 
class M_pengadaan extends CI_Model{

	function get_all_files(){
		$hsl=$this->db->query("SELECT pengadaan.* ,date_pengadaan AS tanggal FROM pengadaan ORDER BY id_pengadaan DESC");
		return $hsl;
	}
	function simpan_file($judul,$file,$pemenang,$unit,$angaran,$masa){
		$hsl=$this->db->query("INSERT INTO pengadaan (judul_pengadaan,spesifikasi_pengadaan,pemenang_lelang,jumlah_unit,angaran,masa_kerja) VALUES ('$judul','$file','$pemenang','$unit','$angaran','$masa')");
		return $hsl;
	}
	function update_file($kode,$judul,$file,$pemenang,$unit,$angaran){
		$hsl=$this->db->query("UPDATE pengadaan SET judul_pengadaan='$judul',spesifikasi_pengadaan='$file',pemenang_lelang='$pemenang',jumlah_unit='$unit',angaran='$angaran' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function update_dukung($kode,$file){
		$hsl=$this->db->query("UPDATE pengadaan SET datadukung_pengadaan='$file' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function update_harga($kode,$file){
		$hsl=$this->db->query("UPDATE pengadaan SET dokumen_harga='$file' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function update_pengumuman($kode,$file){
		$hsl=$this->db->query("UPDATE pengadaan SET pengumuman_pengadaan='$file' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function update_pemenang($kode,$file){
		$hsl=$this->db->query("UPDATE pengadaan SET pengumuman_pemenang='$file' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function update_usulan($kode,$file){
		$hsl=$this->db->query("UPDATE pengadaan SET usulan_pemenang='$file' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function update_pemenangppk($kode,$file){
		$hsl=$this->db->query("UPDATE pengadaan SET pengumuman_pemenang_ppk='$file' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function update_kontrak($kode,$file){
		$hsl=$this->db->query("UPDATE pengadaan SET kontrak_ppk='$file' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function update_spk($kode,$file){
		$hsl=$this->db->query("UPDATE pengadaan SET spk_ppk='$file' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function update_hasil($kode,$file){
		$hsl=$this->db->query("UPDATE pengadaan SET laporan_hasil='$file' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function update_addendum($kode,$file){
		$hsl=$this->db->query("UPDATE pengadaan SET laporan_addendum='$file' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function update_akhir($kode,$file){
		$hsl=$this->db->query("UPDATE pengadaan SET laporan_akhir='$file' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function update_acara($kode,$file){
		$hsl=$this->db->query("UPDATE pengadaan SET berita_acara='$file' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function update_dokumen($kode,$file){
		$hsl=$this->db->query("UPDATE pengadaan SET risalah_dokumen='$file' WHERE id_pengadaan='$kode'");
		return $hsl;
	}



	function update_file_tanpa_file($kode,$judul,$pemenang,$unit,$angaran,$masa){
		$hsl=$this->db->query("UPDATE pengadaan SET judul_pengadaan='$judul',pemenang_lelang='$pemenang',jumlah_unit='$unit',angaran='$angaran',masa_kerja='$masa' WHERE id_pengadaan='$kode'");
		return $hsl;
	}
	function hapus_file($kode){
		$hsl=$this->db->query("DELETE FROM pengadaan WHERE id_pengadaan='$kode'");
		return $hsl;
	}

	function get_file_byid($id){
		$hsl=$this->db->query("SELECT id_pengadaan,judul_pengadaan,DATE_FORMAT(date_pengadaan,'%d/%m/%Y') AS tanggal,spesifikasi_pengadaan FROM pengadaan WHERE id_pengadaan='$id'");
		return $hsl;
	}

	
}