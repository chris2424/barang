<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Supplier extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();

        $this->load->model('Admin_model', 'admin');
        $this->load->model('m_supplier');
        $this->load->library('upload');
        $this->load->helper('download');
    }

    public function index()
    {
        $data['title'] = "Supplier";
        $data['data']=$this->m_supplier->get_all_files();
        $this->template->load('templates/dashboard', 'supplier', $data);
    }

    public  function simpan_file(){
        $config['upload_path'] = './assets/files/supplier/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan
        $this->upload->initialize($config);
        if(!empty($_FILES['brosur']['name']))
            {
                if ($this->upload->do_upload('brosur'))
                {
                        $gbr = $this->upload->data();
                        $file=$gbr['file_name'];
                        $nama=strip_tags($this->input->post('xnama'));
                        $notelp=strip_tags($this->input->post('xnotelp'));
                        $alamat=strip_tags($this->input->post('xalamat'));
                        $npwp=strip_tags($this->input->post('xnpwp'));
                        $spesifikasi=strip_tags($this->input->post('xspesifikasi'));
                        $harga=strip_tags($this->input->post('xharga'));

                        $this->m_supplier->simpan_file($nama,$notelp,$alamat,$npwp,$spesifikasi,$harga,$file);
                        echo $this->session->set_flashdata('msg','success');
                        redirect('Supplier');
                }else{
                    echo $this->session->set_flashdata('msg','warning');
                    redirect('Supplier');
                }
                
            }else{
            redirect('Supplier');
        
    }
    }

    public function hapus_file(){
		$kode=$this->input->post('kode');
		$data=$this->input->post('file');
		$path='./assets/files/supplier/'.$data;
		unlink($path);
		$this->m_supplier->hapus_file($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('supplier');
    }
    
    public function update_file(){
				
        $config['upload_path'] = './assets/files/supplier/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['brosur']['name']))
        {
            if ($this->upload->do_upload('brosur'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $nama=strip_tags($this->input->post('xnama'));
                        $notelp=strip_tags($this->input->post('xnotelp'));
                        $alamat=strip_tags($this->input->post('xalamat'));
                        $npwp=strip_tags($this->input->post('xnpwp'));
                        $spesifikasi=strip_tags($this->input->post('xspesifikasi'));
                        $harga=strip_tags($this->input->post('xharga'));
                    $data=$this->input->post('file');
                    $path='./assets/files/supplier/'.$data;
                    unlink($path);
                    $this->m_supplier->update_file($kode,$nama,$notelp,$alamat,$npwp,$spesifikasi,$harga,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('supplier');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('supplier');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $nama=strip_tags($this->input->post('xnama'));
                $notelp=strip_tags($this->input->post('xnotelp'));
                $alamat=strip_tags($this->input->post('xalamat'));
                $npwp=strip_tags($this->input->post('xnpwp'));
                $spesifikasi=strip_tags($this->input->post('xspesifikasi'));
                $harga=strip_tags($this->input->post('xharga'));
                $this->m_supplier->update_file_tanpa_file($kode,$nama,$notelp,$alamat,$npwp,$spesifikasi,$harga);
                echo $this->session->set_flashdata('msg','info');
                redirect('supplier');
        } 
    }
}
