<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengadaan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();

        $this->load->model('Admin_model', 'admin');
        $this->load->model('m_pengadaan');
        $this->load->library('upload');
        $this->load->helper('download');
   
    }

    public function index()
    {
        $data['title'] = "Pengadaan";
        $data['data']=$this->m_pengadaan->get_all_files();
        $this->template->load('templates/dashboard', 'pengadaan', $data);
    }

    public  function simpan_file(){
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan
        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
            {
                if ($this->upload->do_upload('filefoto'))
                {
                        $gbr = $this->upload->data();
                        $file=$gbr['file_name'];
                        $judul=strip_tags($this->input->post('xjudul'));
                        $pemenang=strip_tags($this->input->post('xpemenang'));
                        $unit=strip_tags($this->input->post('xunit'));
                        $angaran=strip_tags($this->input->post('xangaran'));
                        $masa=strip_tags($this->input->post('xmasa'));

                        $this->m_pengadaan->simpan_file($judul,$file,$pemenang,$unit,$angaran,$masa);
                        echo $this->session->set_flashdata('msg','success');
                        redirect('Pengadaan');
                }else{
                    echo $this->session->set_flashdata('msg','warning');
                    redirect('Pengadaan');
                }
                
            }else{
            redirect('Pengadaan');
        
    }
    }

    public function update_file(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $judul=strip_tags($this->input->post('xjudul'));
                    $pemenang=strip_tags($this->input->post('xpemenang'));
                        $unit=strip_tags($this->input->post('xunit'));
                        $angaran=strip_tags($this->input->post('xangaran'));
                        $masa=strip_tags($this->input->post('xmasa'));
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_file($kode,$judul,$file,$pemenang,$unit,$angaran,$masa);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $judul=strip_tags($this->input->post('xjudul'));
                $pemenang=strip_tags($this->input->post('xpemenang'));
                        $unit=strip_tags($this->input->post('xunit'));
                        $angaran=strip_tags($this->input->post('xangaran'));
                        $masa=strip_tags($this->input->post('xmasa'));
                $this->m_pengadaan->update_file_tanpa_file($kode,$judul,$pemenang,$unit,$angaran,$masa);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }

    public function update_dukung(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_dukung($kode,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $this->m_pengadaan->update_file_tanpa_file($kode);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }

    public function update_harga(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_harga($kode,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $this->m_pengadaan->update_file_tanpa_file($kode);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }

    public function update_pengumuman(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_pengumuman($kode,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $this->m_pengadaan->update_file_tanpa_file($kode);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }

    public function update_pemenang(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_pemenang($kode,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $this->m_pengadaan->update_file_tanpa_file($kode);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }

    public function update_usulan(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_usulan($kode,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $this->m_pengadaan->update_file_tanpa_file($kode);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }

    public function update_pemenangppk(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_pemenangppk($kode,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $this->m_pengadaan->update_file_tanpa_file($kode);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }

    public function update_kontrak(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_kontrak($kode,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $this->m_pengadaan->update_file_tanpa_file($kode);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }
    public function update_spk(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_spk($kode,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $this->m_pengadaan->update_file_tanpa_file($kode);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }

    public function update_hasil(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_hasil($kode,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $this->m_pengadaan->update_file_tanpa_file($kode);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }

    public function update_addendum(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_addendum($kode,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $this->m_pengadaan->update_file_tanpa_file($kode);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }

    public function update_akhir(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_akhir($kode,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $this->m_pengadaan->update_file_tanpa_file($kode);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }   

    public function update_acara(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_acara($kode,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $this->m_pengadaan->update_file_tanpa_file($kode);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }   

    public function update_dokumen(){
				
        $config['upload_path'] = './assets/files/pengadaan/'; //path folder
        $config['allowed_types'] = 'pdf|doc|docx|ppt|pptx|zip'; //type yang dapat diakses bisa anda sesuaikan


        $this->upload->initialize($config);
        if(!empty($_FILES['filefoto']['name']))
        {
            if ($this->upload->do_upload('filefoto'))
            {
                    $gbr = $this->upload->data();
                    $file=$gbr['file_name'];
                    $kode=$this->input->post('kode');
                    $data=$this->input->post('file');
                    $path='./assets/'.$data;
                    unlink($path);
                    $this->m_pengadaan->update_dokumen($kode,$file);
                    echo $this->session->set_flashdata('msg','info');
                    redirect('Pengadaan');
                
            }else{
                echo $this->session->set_flashdata('msg','warning');
                redirect('Pengadaan');
            }
            
        }else{
                $kode=$this->input->post('kode');
                $this->m_pengadaan->update_file_tanpa_file($kode);
                echo $this->session->set_flashdata('msg','info');
                redirect('Pengadaan');
        } 
    }   

    
   


    public function hapus_file(){
		$kode=$this->input->post('kode');
		$data=$this->input->post('file');
		$path='./assets/files/pengadaan/'.$data;
		unlink($path);
		$this->m_pengadaan->hapus_file($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('pengadaan');
	}



    function download(){
		$id=$this->uri->segment(4);
		$get_db=$this->m_files->get_file_byid($id);
		$q=$get_db->row_array();
		$file=$q['file_data'];
		$path='./assets/files/pengadaan/'.$file;
		$data =  file_get_contents($path);
		$name = $file;

		force_download($name, $data); 
		redirect('files');
	}


    
}
